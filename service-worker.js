var CACHE_NAME = 'hkcota-event-notice-board';
var urlsToCache = [
  'https://events.cota.hk/api/v1/days/hkoscon-2019',
];

self.addEventListener('install', function(event) {
  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        console.log('Opened cache');
        return cache.addAll(urlsToCache);
      })
  );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request)
      .then(function(response) {
        // Cache hit - return response
        if (response) {
          return response;
        }

        // Make sure to do and cache CORS request
        event.request.mode = 'cors';
        return fetch(event.request);
      }
    )
  );
});
