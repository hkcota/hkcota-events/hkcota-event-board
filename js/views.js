/**
 * drawOptionsSelOn
 *
 * Draw a list of button, capture the input when clicked and return
 * the selection with Promise.
 *
 * Example render output:
 *
 * ```html
 * <div class="selection-wrapper">
 *   <form class="selection-form" method="GET">
 *     <ul class="options">
 *       <li class="option"><button name="option" value="Reception Area" type="submit">Reception Area</button></li>
 *       <li class="option"><button name="option" value="Charles K Kao Auditorium" type="submit">Charles K Kao Auditorium</button></li>
 *       <li class="option"><button name="option" value="Conference Hall 4" type="submit">Conference Hall 4</button></li>
 *       <li class="option"><button name="option" value="Conference Hall 5" type="submit">Conference Hall 5</button></li>
 *       <li class="option"><button name="option" value="Conference Hall 6" type="submit">Conference Hall 6</button></li>
 *       <li class="option"><button name="option" value="Conference Hall 7" type="submit">Conference Hall 7</button></li>
 *     </ul>
 *   </form>
 * </div>
 * ```
 *
 * @param {HTMLElement} container
 *     HTML Element to contain the render result.
 * @param {Array.<String>} options
 *    An array of option strings.
 *
 * @return {Promise}
 *    A promise of string selected by user.
 */
function drawOptionsSelOn(container, options) {

  // clear container current contents
  while (container.firstChild) {
    container.firstChild.remove();
  }
  return new Promise((resolve, reject) => {
    // try to render the element into container
    try {
      const wrapper = Object.assign(
        document.createElement('div'),
        {className: 'selection-wrapper'},
      );
      const form = Object.assign(
        document.createElement('form'),
        {className: 'selection-form', method: 'GET'},
      );
      const optionsList = Object.assign(
        document.createElement('ul'),
        {className: 'options'},
      );
      const optionSelected = (evt) => {
        evt.preventDefault();
        // resolve on option button clicked.
        resolve(evt.target.value);
      }
      options.reduce((list, option) => {
        const item = Object.assign(
          document.createElement('li'),
          {
            className: 'option',
          },
        );
        const button = Object.assign(
          document.createElement('button'),
          {
            name: 'option',
            value: option,
            type: 'submit',
            textContent: option,
          },
        );
        button.addEventListener('click', optionSelected);
        item.appendChild(button);
        list.appendChild(item);
        return list;
      }, optionsList);
      form.appendChild(optionsList);
      wrapper.appendChild(form);
      container.appendChild(wrapper);
    } catch (e) {
      reject(e);
    }
  });
}

/**
 * Creates a card HTML for displaying the speaker information.
 *
 * @param {Array.<name: String, thumbnail: String>} speaker
 *     A speaker object.
 *
 * @return {HTMLElement} speakerWrapper
 *     The wrapper element of the rendered HTML.
 */
function createSpeakerCard({name, thumbnail}) {
  let speakerWraper = Object.assign(document.createElement('li'), {
    className: 'event-speaker',
  });
  let speakerCard = Object.assign(document.createElement('figure'), {
    className: 'event-speaker-card',
  });
  let speakerThumbWrapper = Object.assign(document.createElement('div'), {
    className: 'event-speaker-card-thumbnail',
  });
  let speakerThumb = Object.assign(document.createElement('img'), {
    src: thumbnail,
  });
  let speakerName = Object.assign(document.createElement('figcaption'), {
    innerHTML: name,
  });
  speakerThumbWrapper.appendChild(speakerThumb);
  speakerCard.appendChild(speakerThumbWrapper);
  speakerCard.appendChild(speakerName);
  speakerWraper.appendChild(speakerCard);
  return speakerWraper;
}

/**
 * drawNoticeOn
 *
 * Render a event display for notice board.
 *
 * Example render result:
 *
 * ```html
 * <div class="event">
 *   <div class="event-header-wrapper">
 *     <div class="event-header">
 *       <div class="event-text-before-title">Now</div>
 *       <h1 class="event-title">
 *         Ice-cream robot: creating QR code ice-cream coupons with GravityForms (and minimum coding)
 *       </h1>
 *     </div>
 *   </div>
 *   <ul class="event-speakers">
 *     <li class="event-speaker">
 *       <figure class="event-speaker-card">
 *         <div class="event-speaker-card-thumbnail">
 *           <img src="https://events.cota.hk/sites/default/files/2019-03/Judy%20Wong_0.jpg" />
 *         </div>
 *         <figcaption>Judy Wong</figcaption>
 *       </figure>
 *     </li>
 *   </ul>
 * </div>
 * ```
 *
 * @param {HTMLElement} container
 *     HTML Element to contain the render result.
 * @param {String} title
 *     Title of the event notice.
 * @param {Array.<name: String, thumbnail: String>} speakers
 *     Array of information about speakers of the event.
 */
function drawNoticeOn(container, title, speakers, beforeTitle) {
  const wrapper = Object.assign(
    document.createElement('div'),
    { className: 'event' },
  );

  const headerWrapper = Object.assign(
    document.createElement('div'),
    { className: 'event-header-wrapper' },
  );
  const header = Object.assign(
    document.createElement('div'),
    { className: 'event-header' },
  );

  let textBeforeTitle;
  if (beforeTitle !== '') {
    textBeforeTitle = Object.assign(
      document.createElement('div'),
      { className: 'event-text-before-title', textContent: beforeTitle},
    );
  }

  // create title element
  const noticeTitle = Object.assign(
    document.createElement('h1'),
    { className: 'event-title', textContent: title},
  );

  // create speakers element
  const speakerList = Object.assign(
    document.createElement('ul'),
    { className: 'event-speakers' },
  );
  speakers.reduce((speakerList, speaker) => {
    speakerList.appendChild(createSpeakerCard(speaker));
    return speakerList;
  }, speakerList);

  if (typeof textBeforeTitle !== 'undefined') {
    header.appendChild(textBeforeTitle);
  }
  header.appendChild(noticeTitle);
  headerWrapper.appendChild(header);

  wrapper.appendChild(headerWrapper);
  wrapper.appendChild(speakerList);

  // clear container current contents
  // then add the rendered wrapper.
  while (container.firstChild) {
    container.firstChild.remove();
  }
  container.appendChild(wrapper);
}

/**
 * drawNonEventNoticeOn
 *
 * Draws a notice inside the given container.
 *
 * @param {HTMLElement} container
 *   Container element to be drawn inside of.
 * @param {String} title
 *   Plain text title on top
 * @param {String} message
 *   HTML message.
 */
function drawNonEventNoticeOn(container, title, message) {
  const wrapper = Object.assign(
    document.createElement('div'),
    { className: 'notice-wrapper' },
  );
  const notice = Object.assign(
    document.createElement('div'),
    { className: 'notice' },
  );
  const noticeTitle = Object.assign(
    document.createElement('h1'),
    { className: 'notice-title', textContent: title },
  );
  const noticeMessage = Object.assign(
    document.createElement('div'),
    { className: 'notice-message', innerHTML: message },
  );
  notice.appendChild(noticeTitle);
  notice.appendChild(noticeMessage);
  wrapper.appendChild(notice);

  // clear container current contents
  // then add the rendered wrapper.
  while (container.firstChild) {
    container.firstChild.remove();
  }
  container.appendChild(wrapper);
}

/**
 * Draw timeline inside container.
 *
 * ```html
 * <div class="timeline-wrapper">
 *   <div class="timeline">
 *     <div class="timeline-past">
 *       <span class="timeline-start"></span>
 *       <span class="timeline-time"></span>
 *     </div>
 *     <div class="timeline-remaining">
 *       <span class="timeline-end"></span>
 *     </div>
 *   </div>
 * </div>
 * ```
 *
 * @param {function} calc
 *   A function to take startTme, endTime and
 *   calculate the current time percentage.
 * @param {HTMLElement} container
 *   Container element to be drawn inside of.
 * @param {string} startTime
 *   Start time of the timeline.
 * @param {string} endTime
 *   End time of the timeline.
 */
function drawTimelineOn(container, calc, startTime, endTime) {
  const wrapper = Object.assign(
    document.createElement('div'),
    { className: 'timeline-wrapper' },
  );
  const timeline = Object.assign(
    document.createElement('div'),
    { className: 'timeline' },
  );
  const timelinePast = Object.assign(
    document.createElement('div'),
    { className: 'timeline-past' },
  );
  const timelineStart = Object.assign(
    document.createElement('span'),
    { className: 'timeline-start', textContent: startTime },
  );
  const timelineTime = Object.assign(
    document.createElement('span'),
    { className: 'timeline-time' },
  );
  const timelineRemaining = Object.assign(
    document.createElement('div'),
    { className: 'timeline-remaining' },
  );
  const timelineEnd = Object.assign(
    document.createElement('span'),
    { className: 'timeline-end', textContent: endTime },
  );

  timelinePast.appendChild(timelineStart);
  timelinePast.appendChild(timelineTime);
  timelineRemaining.appendChild(timelineEnd);
  timeline.appendChild(timelinePast);
  timeline.appendChild(timelineRemaining);
  wrapper.appendChild(timeline);

  const updateTimeline = () => {
    const pastRatio = calc(startTime, endTime);
    const past = "%.4f%%".format(pastRatio * 100);
    const remaining = "%.4f%%".format((1.0 - pastRatio) * 100);
    timelinePast.style.width = past;
    timelineRemaining.style.width = remaining;
  };

  updateTimeline();
  setInterval(updateTimeline, 5000);
  container.appendChild(wrapper);
}

/**
 * drawTimeSelOn
 *
 * Draw a form with <input type="time"> box and return a
 * Promise of the submitted value. Default value is the
 * "local time" according to browser.
 *
 * Example render output:
 *
 * ```html
 * <div class="time-input-wrapper">
 *   <form class="time-input-form" method="GET">
 *     <label for="time-selection">Some Label</label>
 *     <input id="time-selection" />
 *     <div class="actions">
 *       <button type="submit">Select Time</button>
 *     </div>
 *   </form>
 * </div>
 * ```
 *
 * @param {HTMLElement} container
 *     HTML Element to contain the render result.
 * @param {Array} renderOptions
 *    An array of render options.
 *
 * @return {Promise}
 *    A promise of string selected by user.
 */
function drawTimeSelOn(container, renderOptions) {
  // clear container current contents
  while (container.firstChild) {
    container.firstChild.remove();
  }
  return new Promise((resolve, reject) => {

    const options = Object.assign({
      label: 'Select Time',
      buttonText: 'Submit',
    }, renderOptions);

    const now = new Date();
    const nowTime = '%02d:%02d'.format(now.getHours(), now.getMinutes());

    // try to render the element into container
    try {
      const wrapper = Object.assign(
        document.createElement('div'),
        {className: 'time-input-wrapper'},
      );
      const form = Object.assign(
        document.createElement('form'),
        {className: 'time-input-form', method: 'GET'},
      );
      const label = Object.assign(
        document.createElement('label'),
        {htmlFor: 'time-selection', textContent: options.label},
      );
      const input = Object.assign(
        document.createElement('input'),
        {type: 'time', id: 'time-selection', value: nowTime},
      );
      const actions = Object.assign(
        document.createElement('div'),
        {className: 'actions'},
      );
      const submit = Object.assign(
        document.createElement('button'),
        {type: 'submit', textContent: options.buttonText},
      );

      actions.append(submit);
      form.appendChild(label);
      form.appendChild(input);
      form.appendChild(actions);
      wrapper.appendChild(form);
      container.appendChild(wrapper);

      form.addEventListener('submit', (evt) => {
        evt.preventDefault();
        resolve(input.value);
      });
    } catch (e) {
      reject(e);
    }
  });
}

export default {
  drawOptionsSelOn,
  drawNoticeOn,
  drawNonEventNoticeOn,
  drawTimelineOn,
  drawTimeSelOn,
};

export {
  drawOptionsSelOn,
  drawNoticeOn,
  drawNonEventNoticeOn,
  drawTimelineOn,
  drawTimeSelOn,
};
