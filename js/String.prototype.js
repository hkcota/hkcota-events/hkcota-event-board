/**
 * String.prototype.format()
 *
 * A prototype method to mimic printf.
 *
 * Apply to String prototype  :
 *   // Use extra String.prototype function "format".
 *   Object.assign(String.prototype, {
 *     format,
 *   });
 *
 * Usage:
 *   >> "some string: %06d %s".format(123, hello)
 *   "some string: 000123 hello"
 *
 * Reference:
 *   https://stackoverflow.com/posts/13439711/revisions
 */
function format() {
  var i = -1;
  const args = arguments;
  const regex = /%(-)?(0?[0-9]+)?([.][0-9]+)?([#][0-9]+)?([scfpexd%])/g;
  return this.replace(regex, function(exp, p0, p1, p2, p3, p4) {
    if (exp=='%%') return '%';
    if (args[++i] === undefined) return undefined;
    exp  = p2 ? parseInt(p2.substr(1)) : undefined;
    const base = p3 ? parseInt(p3.substr(1)) : undefined;
    let val;
    switch (p4) {
      case 's': val = args[i]; break;
      case 'c': val = args[i][0]; break;
      case 'f': val = parseFloat(args[i]).toFixed(exp); break;
      case 'p': val = parseFloat(args[i]).toPrecision(exp); break;
      case 'e': val = parseFloat(args[i]).toExponential(exp); break;
      case 'x': val = parseInt(args[i]).toString(base?base:16); break;
      case 'd': val = parseFloat(parseInt(args[i], base?base:10).toPrecision(exp)).toFixed(0); break;
    }
    val = typeof(val) == 'object' ? JSON.stringify(val) : val.toString(base);
    const sz = parseInt(p1); /* padding size */
    const ch = (p1 && p1[0] == '0') ? '0' : ' '; /* isnull? */
    while (val.length<sz) val = (p0 !== undefined) ? val+ch : ch+val; /* isminus? */
    return val;
  });
}

export default {
  format,
};

export {
  format,
};
