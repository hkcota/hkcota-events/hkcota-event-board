function flattenTopics(days) {
  const topics = [];
  for (let {day, date, timeslots} of days) {
    for (let {startTime, endTime, events} of timeslots) {
      for (let {topic, display, language, language_sym_interp, speakers, venue: {name: venue}} of events) {
        topics.push({
          day,
          date,
          startTime,
          endTime,
          topic,
          display,
          language,
          language_sym_interp,
          venue,
          speakers: speakers.map(({name, thumbnail}) => ({name, thumbnail})),
        });
      }
    }
  }
  return topics;
}

function reduceVenues(topics) {
  const venues = {};
  topics.reduce((accu, {venue}) => {
    if (typeof venue === 'string' && venue !== '')
    accu[venue] = true;
    return accu;
  }, venues);
  return Object.getOwnPropertyNames(venues);
}

function isTopic(value=true) {
  return ({topic}) => (topic === value);
}

function byDate(dateWanted) {
  return ({date}) => (dateWanted === date);
}

function byStartBefore(time) {
  return ({startTime}) => (startTime <= time);
}

function byEndAfter(time) {
  return ({endTime}) => (time < endTime);
}

function byTime(time) {
  return ({startTime, endTime}) => (startTime <= time && time < endTime);
}

function byVenue(name) {
  return ({venue}) => (venue === name);
}

export {
  flattenTopics,
  reduceVenues,
  isTopic,
  byDate,
  byStartBefore,
  byEndAfter,
  byTime,
  byVenue,
}
