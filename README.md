# HKOSCon Event Board

A simple HTML5 implementation displaying event name on a Kiosk screen.
For displaying event that is scheduled to happen in a given room.

## Features
* Allow user to select the conference track by interactively asking the conference year,
  the venue (room).
* If the current date and time does not match the event date and time, will assume to be
  a testing attempt. Will allow user to select the mock time.
* Automatically get the latest event information from API upstream.
* Automatically change the event name according to schedule.

## Live Version
A live version of the latest commit here is hosted at GitLab Pages:
* https://hkcota.gitlab.io/hkcota-events/hkcota-event-board/
